<?php

// filter: tiny_mce_before_init
function setEditorColors($init)
{

}

//filter:mce_buttons_2
function add_editor_styleselect_btn($buttons)
{
    array_unshift($buttons, 'styleselect');
    return $buttons;
}
add_filter('mce_buttons_2', 'add_editor_styleselect_btn');

function set_custom_editor_stylesheet()
{
    add_editor_style('assets/css/wp-editor-style.css');
}

add_action('admin_init', 'set_custom_editor_stylesheet');

//filter:tiny_mce_before_init
function setup_custom_editor_settings($init_array)
{

    // Define the style_formats array
    $style_formats = array(
        array(
            'title' => 'Buttons',
            'items' => array(
                array(
                    'title'    => 'Button',
                    'selector' => 'a, button',
                    'classes'  => 'btn',
                ),
                array(
                    'title'    => 'Fairlingo Mint',
                    'selector' => '.btn',
                    'classes'  => 'btn-primary',
                ),
                array(
                    'title'    => 'Fairlingo Yellow',
                    'selector' => '.btn',
                    'classes'  => 'btn-secondary',
                ),
                array(
                    'title'    => 'Border only',
                    'selector' => '.btn',
                    'classes'  => 'btn-outline',
                ),
                array(
                    'title'    => 'Large',
                    'selector' => '.btn',
                    'classes'  => 'btn-lg',
                ),
                array(
                    'title'    => 'Extra large',
                    'selector' => '.btn',
                    'classes'  => 'btn-extra-lg',
                ),
                array(
                    'title'    => 'Full width',
                    'selector' => '.btn',
                    'classes'  => 'full-width',
                ),
            ),
        ),
        array(
            'title' => 'Heading styles',
            'items' => array(
                array(
                    'title'    => 'Uppercase',
                    'selector' => '*',
                    'classes'  => 'uppercase',
                ),
                array(
                    'title'    => 'Heading 1 size',
                    'selector' => 'h1,h2,h3,h4,h5,h6,p',
                    'classes'  => 'h1',
                ),
                array(
                    'title'    => 'Heading 2 size',
                    'selector' => 'h1,h2,h3,h4,h5,h6,p',
                    'classes'  => 'h2',
                ),
                array(
                    'title'    => 'Heading 3 size',
                    'selector' => 'h1,h2,h3,h4,h5,h6,p',
                    'classes'  => 'h3',
                ),
                array(
                    'title'    => 'Heading 4 size',
                    'selector' => 'h1,h2,h3,h4,h5,h6,p',
                    'classes'  => 'h4',
                ),
                array(
                    'title'    => 'Heading 5 size',
                    'selector' => 'h1,h2,h3,h4,h5,h6,p',
                    'classes'  => 'h5',
                ),
                array(
                    'title'    => 'Heading 6 size',
                    'selector' => 'h1,h2,h3,h4,h5,h6,p',
                    'classes'  => 'h6',
                ),
            ),
        ),
    );

    //change default colors
    $default_colours = '[
	    "000000", "Black",
	    "ffffff", "White",
	    "F2F2F2", "Light Gray",
	    "3f3c3a", "Dark Gray",
	    "22365D", "Code.Rehab Blue",
	    "5D729A", "Code.Rehab light Blue"
	]';

    $init_array['textcolor_map']  = $default_colours;
    $init_array['textcolor_rows'] = 1;
    $init_array['style_formats']  = json_encode($style_formats);

    return $init_array;
}
add_filter('tiny_mce_before_init', 'setup_custom_editor_settings');

function siteorigin_universal_color_pallet()
{
    ?>
    <script>
        jQuery(document).ready(function($){
            $.wp.wpColorPicker.prototype.options = {
                palettes: ['#000000', '#ffffff', '#F2F2F2', '#3f3c3a','#22365D', '#5D729A']
            };
        });
    </script>
<?php
}
add_action('admin_print_footer_scripts', 'siteorigin_universal_color_pallet');
add_action('customize_controls_print_footer_scripts', 'siteorigin_universal_color_pallet');
