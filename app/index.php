<?php
// namespace Theme;
use Theme\Controllers\PageController;
use Theme\Controllers\PostController;
use Theme\Controllers\WPController;

function getTemplate()
{
    if (is_search() || is_404()) {
        return new WPController();
    }

    switch (get_post_type()) {
        case "post":
            return new PostController();
            break;

        case "page":
            return new PageController();
            break;
    }
}

$template = getTemplate();

if (is_404()) {
    echo $template->error404();
    exit;
}

if (is_search()) {
    echo $template->search();
    exit;
}

if (is_category()) {
    echo $template->category();
    exit;
}

if (is_tag()) {
    echo $template->tag();
    exit;
}

if (is_archive()) {
    echo $template->archive();
    exit;
}

if (is_single() || is_page()) {
    echo $template->single();
    exit;
}
