<?php
namespace Theme\Controllers;
use Theme\Controllers\WPBaseController;

class WPController extends WPBaseController {

    // public function error404()
    // {
    //   $props = [];
    //   // define stuff
    //   return $this->render('error-404', $props);
    // }

    public function search()
    {
      $props = [];
      $props['searchquery'] = ">> " . get_search_query() . " <<";
      return $this->render('search', $props);
    }

}
