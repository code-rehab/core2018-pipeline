<nav id="main-menu">
  <div class="inner">
    {{ wp_nav_menu( array('theme_location' => 'main-menu') ) }}
  </div>
</nav>

<div class="btn toggle-mainmenu-button d-md-none">
  <i class="fas fa-bars open-button"></i>
  <i class="fa fa-times close-button"></i>
</div>
