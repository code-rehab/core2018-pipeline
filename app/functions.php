<?php
namespace Theme;
require 'vendor/autoload.php';

use Philo\Blade\Blade;

if (!file_exists(__DIR__ . '/view')) {
    mkdir(__DIR__ . '/view', 0755, true);
}

if (!file_exists(__DIR__ . '/cache')) {
    mkdir(__DIR__ . '/cache', 0755, true);
}

$views = __DIR__ . '/view';
$cache = __DIR__ . '/cache';

global $blade;
$blade = new Blade($views, $cache);

// Language
load_theme_textdomain( 'core', get_template_directory() . '/include/languages' );

// Functions
require_once "include/function/scripts-styles.php";
require_once "include/function/menus.php";
require_once "include/function/theme-support.php";
require_once "include/function/basic-security.php";
require_once "include/function/editor-stylesheet.php";
require_once "include/function/no-emoji.php";
require_once "include/function/mimetypes.php";
require_once "include/function/bootstrap-gallery/bootstrap-gallery.php";
require_once "pagebuilder/pagebuilder.php";

//require_once "partials/map/acf-map.php";

// Shortcode
// require_once "include/shortcode/example/example.widget.php";

// Widgets
// require_once "include/widget/example/default.widget.php";

// Posttypes
//require_once "include/posttype/example.ptype.php";
