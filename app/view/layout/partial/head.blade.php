<head>
  <meta charset="{{bloginfo( 'charset' ) }}" />
  <meta name="author" content="{{ bloginfo('name') }}" />
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
  {{wp_head() }}
</head>
