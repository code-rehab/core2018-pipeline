<header id="navigation">
    <div class="row">
      <div class="col-sm-3 col-9 d-flex align-items-end">
        <a id="logo" href="{{ bloginfo('siteurl') }}">
          <img src="{{bloginfo('template_url') }}/assets/images/logo.svg"/>
        </a>
      </div>
      <div class="col-sm-9 col-3 d-flex align-items-center justify-content-end">
        @include('layout.partial.navigation')
      </div>
    </div>
</header>
