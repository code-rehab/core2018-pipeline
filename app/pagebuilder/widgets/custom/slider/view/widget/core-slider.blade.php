
<section class="widget widget-slider" data-flickity='{
  "cellAlign": "left",
  "wrapAround": true,
  "prevNextButtons": {{$showArrows}},
  "pageDots": {{$showBullets}},
  "autoPlay": {{$autoPlay}}
}'>

@foreach ($slides as $slide)
    @php($image = siteorigin_widgets_get_attachment_image_src($slide["image"], 'banner-image', '')[0])
	  <article class="col-12 d-flex align-items-center justify-content-center" style="background-image: url('{{$image}}')">
	    <div class="content">
	      <h2>{{ $slide["title"] }}</h2>
	    </div>
	  </article>
 @endforeach

@if($showFilter)
  <div class="filter"></div>
@endif

</section>
