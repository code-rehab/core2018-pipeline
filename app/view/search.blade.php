@extends('layout.default')

@section("page-content")

  <h2>Searched for: {{$searchquery}}</h2>
  <hr />

  @while(have_posts())
    {{ the_post() }}

    
    {{ the_content() }}
  @endwhile

@endsection
