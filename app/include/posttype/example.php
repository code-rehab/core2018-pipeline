<?php

add_action( 'init', 'create_post_type_default');
function create_post_type_default() {

  $name = __('Defaults', 'core');
  $singular = __('default', 'core');
  $plural = __('defaults', 'core');

  $lowname = strtolower($name);

  register_post_type(
    $lowname,
    array(
      'labels' => array(
        'name'               => $name,
        'singular_name'      => $singular,
        'all_items'          => sprintf( __( 'All %s', 'core' ), $plural ),
        'add_new'            => sprintf( __( 'Add new %s', 'core' ), $singular ),
        'add_new_item'       => sprintf( __( 'Add new %s', 'core' ), $singular ),
        'edit'               => __( 'edit', 'core' ),
        'edit_item'          => sprintf( __( 'edit %s', 'core' ), $singular ),
        'new_item'           => sprintf( __( 'New %s', 'core' ), $singular ),
        'view'               => sprintf( __( 'View %s', 'core' ), $singular ),
        'view_item'          => sprintf( __( 'View %s', 'core' ), $singular ),
        'search_items'       => sprintf( __( 'Search %s', 'core' ), $plural ),
        'not_found'          => sprintf( __( 'No %s'. ' found', 'core' ), $plural ),
        'not_found_in_trash' => sprintf( __( 'No %s'. ' found in trash', 'core' ), $plural ),
        'parent'             => sprintf( __( 'Parent %s', 'core' ), $singular ),
      ),
      'public' => true,
      'has_archive' => true,
      'hierarchical' => true,
      'rewrite' => array(
        'slug'       => $plural,
        'with_front' => false,
      ),
      'show_in_nav_menus' => true,

      'menu_position' => 5, // Onder berichten plaatsen
      'menu_icon'           => 'dashicons-location',

      'rewrite' => array(
        'slug' => $lowname,
        'with_front' => false
      ),

      'supports' => array(
        'title',
        'editor',
        'thumbnail',
        'excerpt',
        'page-attributes'
      ),
    )
  );
  flush_rewrite_rules();
}

// add_action( 'init', 'build_taxonomies_defaults', 0 );
// function build_taxonomies_defaults() {
//   register_taxonomy(
//     'cat_defaults',
//     'defaults',  // this is the custom post type(s) I want to use this taxonomy for
//     array(
//       'hierarchical' => true,
//       'label' => '__('Categories', 'core'),
//       'query_var' => true,
//       'rewrite' => true
//     )
//   );
// }
