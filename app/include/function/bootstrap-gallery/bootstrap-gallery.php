<?php
function bootstrap_gallery( $output = '', $atts, $instance ){

  $return = '
  <script type="text/javascript">
  	$("[data-fancybox]").fancybox({
  		// Options will go here
      // http://fancyapps.com/fancybox/3/docs/
  	});
  </script>
  ';

  $imagesize = $atts['size'];
  $order = $atts['orderby'];

  if (strlen($atts['columns']) < 1) {
    $columns = 3;
  }
  else {
    $columns = $atts['columns']; 
  }

  $images = explode(',', $atts['ids']);
  if ($columns < 1 || $columns > 12) {
    $columns == 3;
  }

  $col_class = 'col-sm-12 col-md-6 col-lg-' . 12/$columns; // only use user input columns for large screens

  $return .= '<div class="row gallery">';
  $i = 0;

  if($order == "rand"){ // if random option is checked
    shuffle($images);
  }

  foreach ($images as $key => $value) {
    if ($i%$columns == 0 && $i > 0) {
      $return .= '</div><div class="row gallery">';
    }
    $image_attributes = wp_get_attachment_image_src($value, $imagesize);
    $full_image = wp_get_attachment_image_src($value, 'full-size');
    $return .= '
    <div class="'.$col_class.'">
      <div class="gallery-image-wrap mb-4">
        <a class="fancybox" data-fancybox="gallery-' . $atts['ids'] . '" href="'.$full_image[0].'">
          <img src="'.$image_attributes[0].'" alt="" class="img-responsive">
        </a>
      </div>
    </div>';
    $i++;
  }
  $return .= '</div>';
  return $return;
}
add_filter( 'post_gallery', 'bootstrap_gallery', 10, 4);
