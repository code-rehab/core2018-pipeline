@extends('layout.default')

@section("page-title")
  <h1>{{ the_title() }}</h1>
@endsection

@section("page-content")

  @while(have_posts())
    @php(the_post())
    {{ the_content() }}
  @endwhile

@endsection
