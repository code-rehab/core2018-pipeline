<?php
namespace Theme\Controllers;

class WPBaseController {
  protected function render($view, $props) {
    global $blade;
    return $blade->view()->make($view, $props)->render();
  }

  public function single()
  {
    $props = [];
    // define stuff
    return $this->render('single.default', $props);
  }

  public function archive()
  {
    $props = [];
    // define stuff
    return $this->render('archive.default', $props);
  }

  public function tag()
  {
    $props = [];
    // define stuff
    return $this->render('archive.default', $props);
  }

  public function category()
  {
    $props = [];
    // define stuff
    return $this->render('archive.default', $props);
  }

  public function error404()
  {
    $props = [];
    // define stuff
    return $this->render('error-404', $props);
  }

  public function search()
  {
    $props = [];
    // define stuff
    return $this->render('search', $props);
  }
}
