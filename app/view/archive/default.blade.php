@extends('layout.default')

@section("page-content")

  @while(have_posts())
    @php(the_post())

    <h1>{{ the_title() }}</h1>

    {{ the_excerpt() }}
  @endwhile

@endsection
