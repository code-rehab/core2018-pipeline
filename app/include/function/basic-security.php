<?php

// Settings
//show_admin_bar( false ); // true shows the adminbar to visitors too

add_filter( 'auto_update_plugin', '__return_false' );
add_filter( 'auto_update_theme', '__return_false' );
add_filter('xmlrpc_enabled', '__return_false');

// Move to files elsewhere
// define( 'WP_AUTO_UPDATE_CORE', false ); // should be placed in wp-config
// define( 'DISALLOW_FILE_EDIT', true ); // should be placed in wp-config
// Options -Indexes // should be placed on bottom of .htaccess

// Functions
require_once "security/remove-actions.php";
//require_once "security/smart-jquery-inclusion.php";
require_once "security/disable-pingbackscanner.php";
require_once "security/remove-feeds.php";
require_once "security/clean-dashboard.php";
