<div class="btn-wrapper" style="text-align:{{ $design['align'] }}">
  <a href="{{sow_esc_url($url)}}" class="btn {{$design['size']}} {{$design['theme']}}"/>{{$text}} {!! siteorigin_widget_get_icon( $button_icon['icon_selected'] ) !!}</a>
</div>
