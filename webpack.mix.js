const mix = require("laravel-mix");

const themeName = "theme";
const themeLocation = "build/wp-content/themes/coderehab-" + themeName;

mix.options({
  publicPath: "./", //windows can't mix without this!
  extractVueStyles: false,
  processCssUrls: false,
  uglify: {},
  purifyCss: false,
  postCss: [require("autoprefixer")],
  clearConsole: true
});

mix
  //compile js
  .js("resources/js/site.js", "app/assets/js")

  .extract(["jquery", "tether", "lodash", "bootstrap", "axios"])

  .autoload({
    jquery: ["$", "jQuery", "window.jQuery"],
    tether: ["Tether", "window.Tether"]
  })

  //compile styles
  .sass("resources/sass/site.scss", "app/assets/css")
  .sass("resources/sass/wp-editor-style.scss", "app/assets/css")
  .sass("resources/sass/includes.scss", "app/assets/css")
  .sass("resources/sass/adminlogin.scss", "app/assets/css")

  .sourceMaps()

  //copy directories
  .copyDirectory("app", themeLocation)
  .copyDirectory("vendor", themeLocation + "/vendor")

  //copy files
  //.copy("node_modules/font-awesome/fonts", themeLocation + "/assets/fonts");

  // Live reloading
  .browserSync(
    {
      proxy: "coderehab.homestead",
      files: ["build/**/*"]
    },
    {
      injectCss: true
    }
  );
